- [I. Prise en main](#i-prise-en-main)
  - [1. Lancer des conteneurs](#1-lancer-des-conteneurs)
    - [Manipulation du conteneur](#manipulation-du-conteneur)
    - [Lancer un conteneur NGINX](#lancer-un-conteneur-nginx)
  - [2. Gestion d'images](#2-gestion-dimages)
    - [Récupérer une image de Apache en version 2.2](#récupérer-une-image-de-apache-en-version-22)
    - [Création d'image](#création-dimage)
  - [3. Manipulation du démon docker](#3-manipulation-du-démon-docker)
    - [Modifier la création du démon docker](#modifier-la-création-du-démon-docker)
- [II. docker-compose](#ii-docker-compose)
    - [docker-compose-v1.yml](#docker-compose-v1yml)
    - [docker-compose-v2.yml](#docker-compose-v2yml)
    - [Conteneuriser une application donnée](#conteneuriser-une-application-donnée)
 

# I. Prise en main

## 1. Lancer des conteneurs

### Manipulation du conteneur

🌞 lister les conteneurs actifs, et mettre en évidence le conteneur lancé sur le sleep, trouver son nom et ID:
```
[vagrant@centos7 ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND        CREATED         STATUS         PORTS     NAMES
775e2ed6145e   alpine    "sleep 9999"   4 minutes ago   Up 4 minutes             thirsty_wright
```

🌞 Mise en évidence d'une partie de l'isolation mise en place par le conteneur. Montrer que le conteneur utilise :
* une arborescence de processus différente
* des cartes réseau différentes
* des utilisateurs système différents
* des points de montage (les partitions montées) différents
```
[vagrant@centos7 ~]$ ps -A
  PID TTY          TIME CMD
    1 ?        00:00:01 systemd
    2 ?        00:00:00 kthreadd
    4 ?        00:00:00 kworker/0:0H
    5 ?        00:00:00 kworker/u4:0
    6 ?        00:00:00 ksoftirqd/0
    7 ?        00:00:00 migration/0
    8 ?        00:00:00 rcu_bh
    9 ?        00:00:00 rcu_sched
   10 ?        00:00:00 lru-add-drain
   ...
   ...

[vagrant@centos7 ~]$ ip l
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP mode DEFAULT group default qlen 1000
    link/ether 08:00:27:19:b4:38 brd ff:ff:ff:ff:ff:ff
3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP mode DEFAULT group default qlen 1000
    link/ether 08:00:27:c9:c1:65 brd ff:ff:ff:ff:ff:ff
4: docker0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP mode DEFAULT group default 
    link/ether 02:42:b9:04:07:c8 brd ff:ff:ff:ff:ff:ff
6: veth69aaa89@if5: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master docker0 state UP mode DEFAULT group default 
    link/ether 6a:f6:3a:0b:e7:73 brd ff:ff:ff:ff:ff:ff link-netnsid 0

[vagrant@centos7 ~]$ cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt
mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
operator:x:11:0:operator:/root:/sbin/nologin
games:x:12:100:games:/usr/games:/sbin/nologin
ftp:x:14:50:FTP User:/var/ftp:/sbin/nologin
nobody:x:99:99:Nobody:/:/sbin/nologin
systemd-network:x:192:192:systemd Network Management:/:/sbin/nologin
dbus:x:81:81:System message bus:/:/sbin/nologin
polkitd:x:999:998:User for polkitd:/:/sbin/nologin
sshd:x:74:74:Privilege-separated SSH:/var/empty/sshd:/sbin/nologin
postfix:x:89:89::/var/spool/postfix:/sbin/nologin
chrony:x:998:996::/var/lib/chrony:/sbin/nologin
vagrant:x:1000:1000::/home/vagrant:/bin/bash
vboxadd:x:997:1::/var/run/vboxadd:/sbin/nologin

[vagrant@centos7 ~]$ df
Filesystem                      1K-blocks    Used Available Use% Mounted on
devtmpfs                           929360       0    929360   0% /dev
tmpfs                              940964       0    940964   0% /dev/shm
tmpfs                              940964   17028    923936   2% /run
tmpfs                              940964       0    940964   0% /sys/fs/cgroup
/dev/mapper/centos_centos7-root  52403200 1962412  50440788   4% /
/dev/sda1                         1038336  133232    905104  13% /boot
tmpfs                              188196       0    188196   0% /run/user/1000
tmpfs                              188196       0    188196   0% /run/user/0

```

🌞 détruire le conteneur avec docker rm
```
[vagrant@centos7 ~]$ docker stop thirsty_wright 
thirsty_wright

[vagrant@centos7 ~]$ docker rm thirsty_wright 
thirsty_wright
```

### Lancer un conteneur NGINX

🌞 Lancer un conteneur NGINX
* utiliser l'image nginx
* lancer le conteneur en démon
* utiliser -p pour partager un port de l'hôte vers le port 80 du conteneur
* visiter le service web en utilisant l'IP de l'hôte (= en utilisant votre partage de port)

```
[vagrant@centos7 ~]$ docker run -d -p 8080:80 nginx
Unable to find image 'nginx:latest' locally
latest: Pulling from library/nginx
a076a628af6f: Pull complete 
0732ab25fa22: Pull complete 
d7f36f6fe38f: Pull complete 
f72584a26f32: Pull complete 
7125e4df9063: Pull complete 
Digest: sha256:10b8cc432d56da8b61b070f4c7d2543a9ed17c2b23010b43af434fd40e2ca4aa
Status: Downloaded newer image for nginx:latest
f92eab59e75865e1b2086861c5c0b80fd7f0282329b6cd20d99f5895db805d57
```
![Capture Nginx](Images/nginx.png)

## 2. Gestion d'images

### Récupérer une image de Apache en version 2.2

🌞 récupérer une image de Apache en version 2.2
* la lancer en partageant un port qui permet d'accéder à la page d'accueil d'Apache
```
[vagrant@centos7 ~]$ docker pull httpd:2.2
2.2: Pulling from library/httpd
f49cf87b52c1: Pull complete 
24b1e09cbcb7: Pull complete 
8a4e0d64e915: Pull complete 
bcbe0eb4ca51: Pull complete 
16e370c15d38: Pull complete 
Digest: sha256:9784d70c8ea466fabd52b0bc8cde84980324f9612380d22fbad2151df9a430eb
Status: Downloaded newer image for httpd:2.2
docker.io/library/httpd:2.2

[vagrant@centos7 ~]$ docker run -p 8080:80 httpd:2.2
```

![Capture Apache2.2](Images/apache2.2.png)

### Création d'image

🌞 créer une image qui lance un serveur web python. L'image doit :
* se baser sur alpine (clause FROM)
* contenir python3 (clause RUN)
* utiliser la clause EXPOSE afin d'être plus explicite
* contenir une clause WORKDIR afin de spécifier un répertoire de travail (les commandes suivantes seront lancées depuis ce répertoire)
* utiliser COPY pour récupérer un fichier à partager à l'aide du serveur HTTP
* lancer la commande python3 -m http.server 8888 (clause CMD)
```Dockerfile
FROM alpine:latest

RUN apk add --no-cache python3 

EXPOSE  8888

WORKDIR /web/
COPY ./http/ /web/

CMD python3 -m http.server 8888
```

🌞 lancer le conteneur et accéder au serveur web du conteneur depuis votre PC
* avec un docker run et les bonnes options
* il faudra faire un partage de port (-p) pour pouvoir partager le port du conteneur vers un port de l'hôte
* par exemple docker run -p 7777:8080 <IMAGE> permet de partager le port 7777 de l'hôte vers le port 8080 du conteneur (TCP par défaut)
```
[vagrant@centos7 Dockerfile]$ docker build -t python .
Sending build context to Docker daemon   2.56kB
Step 1/6 : FROM alpine:latest
 ---> 7731472c3f2a
Step 2/6 : RUN apk add --no-cache python3
 ---> Using cache
 ---> d688f7251702
Step 3/6 : EXPOSE  8888
 ---> Using cache
 ---> b2ccf1da3cc8
Step 4/6 : WORKDIR /web/
 ---> Using cache
 ---> 0085f4059df8
Step 5/6 : COPY ./http/ /web/
 ---> 6bb5a8abd115
Step 6/6 : CMD python3 -m http.server 8888
 ---> Running in d8f702b17cfa
Removing intermediate container d8f702b17cfa
 ---> ca4a676e97b5
Successfully built ca4a676e97b5
Successfully tagged python:latest

[vagrant@centos7 ~]$ docker run -d -p 8080:8888 python
5d987cd7798275e0b10c6433a2bbe1049c5b9bc228eabd8dabea3b32d187b22e
```
![Capture python1](Images/python1.png)

🌞 utiliser l'option -v de docker run
* c'est un volume Docker
* on utilise les volumes pour partager des fichiers de l'hôte dans le conteneur
* vous devrez monter le répertoire de votre choix, dans le WORKDIR du conteneur

## 3. Manipulation du démon docker

### Modifier la création du démon docker

🌞 Modifier la configuration du démon Docker :
* modifier le socket utilisé pour la communication avec le démon Docker
  * trouvez le path du socket UNIX utilisé par défaut (c'est un fichier docker.sock)
  * utiliser un socket TCP (port TCP) à la place
    * autrement dit, il faut que votre démon Docker écoute sur un IP:PORT plutôt que sur le path d'un socket UNIX local
  * prouver que ça fonctionne en manipulant le démon Docker à travers le réseau (depuis une autre machine)
* modifier l'emplacement des données Docker
  * trouver l'emplacement par défaut (c'est le "data-root")
  * le déplacer dans un répertoire /data/docker que vous créerez à cet effet
* modifier le OOM score du démon Docker
  * renseignez-vous sur l'internet si vous ne savez pas à quoi ça correspond :)
  * expliquer succintement la valeur choisie

# II. docker-compose

### docker-compose-v1.yml

🌞 Ecrire un docker-compose-v1.yml qui permet de :
* lancer votre image de serveur web Python créée en 2.
* partage le port TCP du conteneur sur l'hôte
* faire en sorte que le conteneur soit build automatiquement si ce n'est pas fait
```yaml
version: "3.8"
services:
  python:
    build: ../Dockerfile
    ports:
      - "8080:8888"
```
```
[vagrant@centos7 Dockerfile]$ docker-compose -f docker-compose-v1.yml up --build -d
Building python
Step 1/6 : FROM alpine:latest
 ---> 7731472c3f2a
Step 2/6 : RUN apk add --no-cache python3
 ---> Using cache
 ---> d688f7251702
Step 3/6 : EXPOSE  8888
 ---> Using cache
 ---> b2ccf1da3cc8
Step 4/6 : WORKDIR /web/
 ---> Using cache
 ---> 0085f4059df8
Step 5/6 : COPY ./http/ /web/
 ---> 9f37f03acd44
Step 6/6 : CMD python3 -m http.server 8888
 ---> Running in ece5c850a839
Removing intermediate container ece5c850a839
 ---> 8d15ed34db48

Successfully built 8d15ed34db48
Successfully tagged dockerfile_python:latest
Creating dockerfile_python_1 ... done
```

### docker-compose-v2.yml

🌞 Ajouter un deuxième conteneur docker-compose-v2.yml
* ajouter un conteneur NGINX dans le docker-compose-v2.yml
  * réutiliser un conteneur NGINX existant (pas de nouveau Dockerfile)
* celui-ci doit agir comme reverse proxy vers votre serveur Python
  * il va falloir produire une configuration NGINX
    * la configuration doit être monté avec un volume au lancement du conteneur
    * si vous êtes pas à l'aise avec NGINX et sa config, cf le petit encart en dessous de cette liste
  * la connexion au conteneur NGINX doit se faire en HTTPS
    * le certificat et la clé pour la connexion doivent être générés avant le lancement du conteneur
    * ils sont montés avec un volume au lancement du conteneur
  * le port du conteneur NGINX doit être exposé sur l'hôte sur le port 443
  * le port du serveur web n'est plus exposé sur l'hôte
* utiliser les aliases network pour que vos conteneurs communiquent entre eux
```yaml
version: "3.8"
services:

  certificat:
    image:
    volumes:
      - ./nginx/certs/:/certs/

  python:
    build: ../Dockerfile
    restart: on-failure
    expose:
      - "8888"
    networks:
      internal:
        aliases:

  nginx:
    image: nginx
    restart: on-failure
    volumes:
      - ./nginx/conf/nginx.conf:/etc/nginx/conf.d/test.docker.conf
      - ./nginx/certs/:/certs/
    ports:
      - "443:443"
    networks:
      internal:
        aliases:
    depends_on:
      - certificat
      - python

networks:
  internal:
```

### Conteneuriser une application donnée

🌞 Vous devez :
* construire une image qui
  * contient python3
  * contient l'application et ses dépendances
  * lance l'application au démarrage du conteneur
* écrire un docker-compose.yml
  * contient l'application
  * contient un Redis
    * utilise l'image de library
    * a un alias db
  * contient un NGINX
    * reverse proxy HTTPS vers l'application Web
    * a son port 443 exposé